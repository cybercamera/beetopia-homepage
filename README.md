# Beetopia - Contemplative Bee-keeping and Honey-harvesting Game

Have you ever wanted to plant flowers, tend the garden and keep bees? With
Beetopia, you can!

![Beetopia Screen](https://gitlab.com/cybercamera/beetopia/-/raw/main/video.gif "Beetopia Screen")

## Welcome to Beetopia

Beetopia is a simple game where you can choose the flowers you want to plant,
water them and watch them grow. You can then add bee hives to your garden to
attract different kinds of bees, then sit back as the bees collect pollen from
your garden and make honey in their hives. As the honey accumulates, you can
harvest it.

## Download Beetopia

Beetopia was developed using the fabulous Godot Engine (https://godotengine.org/), so you can clone this repository and hack the code, or you can download prebuilt binaries for Linux, MacOS and Windows from here: https://gitlab.com/cybercamera/beetopia-homepage/-/tree/master/public/binaries Maybe in time an Android build will also be added, if there are enough requests.


Beetopia is free and open source software (GPL3). 


